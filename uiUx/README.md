# UI interface inspiration and ideas

The implemented UI Interface are not my original ideas nor designs. Please see below for the dribble.com link for the mockups
* https://dribbble.com/shots/7150621--Real-Estate
* https://dribbble.com/shots/7494179-Real-Estate-Mobile-Grid
* https://dribbble.com/shots/7268823--Real-Estate-Details
