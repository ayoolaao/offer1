import React, { useEffect } from 'react';
import './app.css';
import { useDispatch } from 'react-redux';
import { fetchHomes } from './redux/homesSlice';
import { HomeScreen } from './screens';

const App = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchHomes());
  }, []); // eslint-disable-line

  return (
    <div className="app">
      <HomeScreen />
    </div>
  );
}

export default App;
