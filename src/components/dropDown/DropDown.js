import React from 'react';
import PropTypes from 'prop-types';
import 'react-widgets/dist/css/react-widgets.css';
import './dropDown.css';
import { DropdownList } from 'react-widgets';
import { useDispatch } from 'react-redux';
import { setSelectedItem } from '../../redux/filteringSlice';

const DropDown = ({ name, dropdownOptions, selectedItem }) => {
  const dispatch = useDispatch();

  const handleOnchange = value => {
    dispatch(setSelectedItem({name, value}))
  }

  return (
    <div className='dropdown'>
      <DropdownList
        containerClassName='dropdown'
        data={dropdownOptions}
        defaultValue={selectedItem}
        onChange={value => handleOnchange(value)}
      />
    </div>
  );
};

DropDown.propTypes = {
  data: PropTypes.arrayOf(PropTypes.string),
  name: PropTypes.string
};

export default DropDown;
