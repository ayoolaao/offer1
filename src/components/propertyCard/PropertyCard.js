import React from 'react';
import PropTypes from 'prop-types';
import './propertyCard.css';
import { useDispatch } from 'react-redux';
import { setSelectedHome } from '../../redux/homesSlice';
import { numberToArea, numberToCurrency } from '../../util'

const PropertyCard = ({ property: home }) => {
  const dispatch = useDispatch();

  const {
    price,
    property: { address, id, numberBaths, numberBedrooms, primaryImageUrl, squareFeet },
  } = home;

  const { addressLine1, city, state } = address;

  const handleClick = event => {
    event.stopPropagation();
    dispatch(setSelectedHome(home))
  }

  return (
    <div className='card' onClick={handleClick} id={id}>
      <div className='card__image'>
        { primaryImageUrl ? <img className='card__image_item' src={primaryImageUrl} alt="" /> : ' ' }
      </div>
      <div className='card__address'>{addressLine1? addressLine1 : ''}</div>
      <div className='card__city-state'>{ city ? city : '' }, { state? state: '' }</div>
      <div className='card__price'>{ numberToCurrency(price) }</div>
      <div className='card__stat'>
         <span className='card__stat__item'>{ numberBedrooms ? `🛏   ${numberBedrooms}` : ''}</span>
         <span className='card__stat__item'>{ numberBaths ? `🛀🏾   ${numberBaths}` : ''}</span>
         <span className='card__stat__item'>{ squareFeet ? `❇️   ${numberToArea(squareFeet)}` : ''}</span>
      </div>
    </div>
  );
};

PropertyCard.propTypes = {
  property: PropTypes.object.isRequired
};

export default PropertyCard;
