import React from 'react';
import { useSelector } from 'react-redux';
import { selectCityOptions, selectPriceRangeOptions, selectBedroomOptions } from '../../redux/filteringSlice';
import './filterHomes.css'
import { DropDown } from '..';

const FilterHomes = () => {
  const { availableOptions: availableCities, selectedItem: selectedCity } = useSelector(selectCityOptions);
  const { availableOptions: availablePriceRanges, selectedItem: selectedPriceRange } = useSelector(selectPriceRangeOptions);
  const { availableOptions: availableBedrooms, selectedItem: selectedBedroom } = useSelector(selectBedroomOptions);

  return (
    <div className='filter-homes'>
      <DropDown name='cityOptions' dropdownOptions={availableCities} selectedItem={selectedCity} />
      <DropDown name='priceRangeOptions' dropdownOptions={availablePriceRanges} selectedItem={selectedPriceRange} />
      <DropDown name='bedroomOptions' dropdownOptions={availableBedrooms} selectedItem={selectedBedroom} />
    </div>
  );
};

export default FilterHomes;
