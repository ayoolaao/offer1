import React from 'react';
import PropTypes from 'prop-types';
import './propertyCards.css';
import { PropertyCard } from '..';

const PropertyCards = ({ availableProperties }) => {
  return (
    <div className='property-cards'>
      { !!availableProperties.length && (
        <div className='property-cards__with-results'>
          { availableProperties.map(property => (<PropertyCard property={property} />)) }
        </div>
      )}
      { !availableProperties.length && ( <div className='property-cards__no-results'>No Results</div> ) }
    </div>
  );
};

PropertyCards.propTypes = {
  availableProperties: PropTypes.array
};

PropertyCards.defaultProps = {
  availableProperties: []
}

export default PropertyCards;

