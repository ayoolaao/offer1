import React, {useState, useEffect} from 'react';
import './propertyDetailsModal.css';
import { useSelector } from 'react-redux';
import { useDispatch } from 'react-redux';
import { getSelectHome, setSelectedHome } from '../../redux/homesSlice';
import {numberToArea, numberToCurrency} from '../../util';
import "react-responsive-modal/styles.css";
import { Modal } from "react-responsive-modal";

const PropertyDetailsModal = () => {
  const selectedHome = useSelector(getSelectHome);
  const [showModal, updateShowModal] = useState(false);
  const dispatch = useDispatch();

  const handleModalClose = () => {
    // TODO set selecedHome to null
    dispatch(setSelectedHome(null));
  };

  useEffect(() => {
    if (selectedHome) {
      updateShowModal(true);
    } else {
      updateShowModal(false);
    }
  }, [ selectedHome ]);

  if (selectedHome) {
    const {
      listingAgent,
      price,
      property: { address, description, numberBaths, numberBedrooms, primaryImageUrl, squareFeet },
    } = selectedHome;

    const { addressLine1, city, state, zip } = address;
    const { user: { firstName, lastName, email }, licenseNumber } = listingAgent;

    return (
      <Modal open={showModal} onClose={handleModalClose} center={true}>
        <div className='property-details-modal'>
          <div className='property-details-modal__details'>
            <div className="property-details-modal__image">
              { primaryImageUrl ? <img className='property-details-modal__image_item' src={primaryImageUrl} alt="" /> : ' ' }
            </div>
            <div className="property-details-modal__stats">
              <span className='property-details-modal__stat__item'>{ numberBedrooms ? `🛏   ${numberBedrooms}` : ''}</span>
              <span className='property-details-modal__stat__item'>{ numberBaths ? `🛀🏾   ${numberBaths}` : ''}</span>
              <span className='property-details-modal__stat__item'>{ squareFeet ? `❇️   ${numberToArea(squareFeet)}` : ''}</span>
            </div>
            <div className="property-details-modal__address">
              <span className="property-details-modal__street">{addressLine1? addressLine1 : ''}, </span>
              <span className="property-details-modal__city-state">{ city ? city : '' }, { state? state: '' }, </span>
              <span className="property-details-modal__city-zipcode">{ zip ? zip : '' }</span>
            </div>
            <div className="property-details-modal__price">{ numberToCurrency(price) }</div>
            <div className="property-details-modal__description">
              <h2 className="property-details-modal__description__header">Description</h2>
              <div className="property-details-modal__description__text">{description}</div>
            </div>
            <div>
              <h2>Agent </h2>
              <p>{firstName} {lastName} (lic: {licenseNumber})</p>
              <a href={`mailto:${email}`} target="_top">Email your agent </a>
            </div>
          </div>
        </div>
      </Modal>
    );
  } else {
    return null;
  }
};

export default PropertyDetailsModal;
