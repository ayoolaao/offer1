import React from 'react';
import './navigationBar.css';

const NavigationBar = ({ isAuthenticated, authenticatedUser }) => {
  return (
    <div className='navigation-bar'>
      <span className='navigation-bar__menu'>🍔</span>
      <span className='navigation-bar__branding'>Offer1</span>
      { isAuthenticated ? (
        <span className='navigation-bar__account navigation-bar__authenticated'>
          <span className='navigation-bar__userid navigation-bar__account__item'>{ authenticatedUser.username }</span>
          <span role='button' className='navigation-bar__sign-out navigation-bar__account__item navigation-bar__button'>Sign Out</span>
        </span>
      ) : (
        <span className='navigation-bar__account navigation-bar__not-authenticated'>
          <span role='button' className='navigation-bar__sign-up navigation-bar__account__item navigation-bar__button'>Sign Up</span>
          <span role='button' className='navigation-bar__sign-in navigation-bar__account__item navigation-bar__button'>Sign In</span>
        </span>
      ) }
    </div>
  );
};

export default NavigationBar;
