import React from 'react';
import './searchBar.css';

const SearchBar = () => {
  return (
    <div className='search-bar'>
      <input type="text" className='search-bar__input' placeholder='Enter an address, neighborhood, city, or ZIP code' />
    </div>
  );
};

export default SearchBar;
