import React from 'react';
import './propertyList.css';
import { FilterHomes, NavigationBar, PropertyCards, SearchBar } from '..';
import PropTypes from 'prop-types';

const PropertyList = ({ availableProperties }) => {
  return (
    <div className='property-list'>
      <NavigationBar />
      <SearchBar />
      <FilterHomes />
      <PropertyCards availableProperties={availableProperties} />
    </div>
  );
};

PropertyList.propTypes = {
  availableProperties: PropTypes.array
};

export default PropertyList;
