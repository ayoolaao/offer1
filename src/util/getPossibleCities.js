const getPossibleCities = (properties=[]) => {
  const cities =  properties.map(property => property.property.address.city)
  return ['All', ...new Set(cities)]
}

export default getPossibleCities;
