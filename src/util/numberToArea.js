const numberToArea= number => new Intl
  .NumberFormat('en-US')
  .format(number) + ' SqFt';

export default numberToArea;
