const processPriceRange = priceRange => {
  let lower, upper;
  const lowerUpperArray = priceRange.split(' - ');

  lower = lowerUpperArray[0] === '0' ? 0 : Number(lowerUpperArray[0].slice(0, -1)) * 1000;
  upper = lowerUpperArray[1] === 'Any Price' ? Infinity : Number(lowerUpperArray[1].slice(0, -1)) * 1000;

  return { lower, upper };
}

const filterProperties = (properties, options={}) => {
  const { city, priceRange, bedroom } = options;
  const { lower: priceRangeLower, upper: priceRangeUpper } = processPriceRange(priceRange);
  const bedroomNumber = Number(bedroom.slice(0, 1));

  if (city === 'All' && !priceRange && !bedroom) return properties

  // TODO Figure out how to intersect these to generate a new unique array (Hacked solution for now)
  const cityFiltered = city === 'All' ? properties : properties.filter(property => property.property.address.city === city);
  const priceRangeFiltered = cityFiltered.filter(property => (property['price'] > priceRangeLower) && (property['price'] <= priceRangeUpper));
  return priceRangeFiltered.filter(property => bedroomNumber >= 3 ? property['property']['numberBedrooms'] >= bedroomNumber : property['property']['numberBedrooms'] === bedroomNumber);
};

export default filterProperties;
