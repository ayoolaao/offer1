import { createSlice } from '@reduxjs/toolkit';
import homesMockData from '../mockData/homes.json';

export const homesSlice =  createSlice({
  name: 'homes',
  initialState: {
    homes: [],
    selectedHome: null
  },
  reducers: {
    setHomes: (state, action) => {
      state.homes = action.payload.availableHomes;

    },
    setSelectedHome: (state, action) => {
      state.selectedHome = action.payload;
    }
  }
})

const { setHomes } = homesSlice.actions;
export const fetchHomes = () => dispatch => {
  setTimeout(() => {
    const data = { availableHomes: homesMockData }

    dispatch(setHomes(data))
  }, 500);
};

export const { setSelectedHome } = homesSlice.actions;

export const selectHomes = state => state.homes.homes;
export const getSelectHome = state => state.homes.selectedHome;

export default homesSlice.reducer;
