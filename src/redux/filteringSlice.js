import { createSlice } from '@reduxjs/toolkit';

export const filteringSlice = createSlice({
  name: 'filteringOptions',
  initialState: {
    cityOptions: {
      availableOptions: ['All'],
      selectedItem: 'All'
    },
    priceRangeOptions: {
      availableOptions: ['0 - 100K', '100K - 299k', '300K - 599k', '600k - Any Price'],
      selectedItem: undefined
    },
    bedroomOptions: {
      availableOptions: ['1 Bed', '2 Beds', '3+ Beds'],
      selectedItem: undefined
    },
  },
  reducers: {
    setAvailableOptions: (state, action) => {
      state[action.payload.name].availableOptions = action.payload.value;
    },
    setSelectedItem: (state, action) => {
      state[action.payload.name].selectedItem = action.payload.value;
    },
  }
});

export const { setAvailableOptions, setSelectedItem } = filteringSlice.actions;

export const selectCityOptions = state => state.filteringData.cityOptions;
export const selectPriceRangeOptions = state => state.filteringData.priceRangeOptions;
export const selectBedroomOptions = state => state.filteringData.bedroomOptions;

export default filteringSlice.reducer;
