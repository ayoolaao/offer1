import { configureStore } from '@reduxjs/toolkit';
import homesReducer from './homesSlice';
import filteringReducer from './filteringSlice';

export default configureStore({
  reducer: {
    homes: homesReducer,
    filteringData: filteringReducer
  }
});
