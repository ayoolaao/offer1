import React, { useEffect, useState } from 'react';
import './homeScreen.css';
import {useDispatch, useSelector} from 'react-redux';
import { selectHomes } from '../../redux/homesSlice';
import { PropertyDetailsModal, PropertyMap, PropertyList } from '../../components';
import {
  selectBedroomOptions,
  selectCityOptions,
  selectPriceRangeOptions,
  setAvailableOptions
} from '../../redux/filteringSlice';
import { filterProperties, getPossibleCities } from '../../util';

const HomeScreen = () => {
  const dispatch = useDispatch()

  const availableProperties = useSelector(selectHomes);
  const { selectedItem: selectedCity } = useSelector(selectCityOptions);
  const { selectedItem: selectedPriceRange } = useSelector(selectPriceRangeOptions);
  const { selectedItem: selectedBedroom } = useSelector(selectBedroomOptions);

  const [ filteredProperties, updateFilteredProperties ] = useState(availableProperties);

  useEffect(() => {
    if (availableProperties && selectedCity && selectedPriceRange && selectedBedroom) {
      const filtered = filterProperties(availableProperties, {city: selectedCity, priceRange: selectedPriceRange, bedroom: selectedBedroom});
      updateFilteredProperties(filtered);
    }
  }, [ availableProperties, selectedCity, selectedPriceRange, selectedBedroom ]);

  useEffect(() => {
    if (availableProperties) {
      const availableCityOptions = getPossibleCities(availableProperties);
      dispatch(setAvailableOptions({ name: 'cityOptions', value: availableCityOptions }))
    }

    updateFilteredProperties(availableProperties)
  }, [ availableProperties ]) // eslint-disable-line

  return (
    <div className='home-screen'>
      <PropertyMap availableProperties={filteredProperties} />
      <PropertyList availableProperties={filteredProperties} />
      <PropertyDetailsModal />
    </div>
  );
};

export default HomeScreen;
